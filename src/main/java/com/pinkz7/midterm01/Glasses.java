/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.midterm01;

/**
 *
 * @author ripgg
 */
public class Glasses {
    protected String name;//ชื่อ
    protected String shape;//ทรงแว่น
    protected String feature;//คุณสมบัติ
    protected String model;//รุ่น
    protected int efficacy=50;//ประสิทธิภาพ
    protected char choose;//เลือก

    Glasses(String name, String shape, String feature, String model) {
        this.name = name;
        this.shape = shape;
        this.feature = feature;
        this.model = model;

        switch (model) {
            case "B":
                efficacy = 60;
                break;
            case "C":
                efficacy = 70;
                break;
            case "D":
                efficacy = 80;
                break;
            case "E":
                efficacy = 90;
                break;
            default:
        }
    }

    public String getN() {//รับชื่อ
        return name;
    }

    public String getS() {//รับทรงแว่น
        return shape;
    }

    public String getF() {//รับคุณสมบัติ
        return feature;
    }

    public String getM() {//รับประสิทธิภาพ
        return model;
    }

    public int setG(String name, String shape, String feature, String model, char choose) {
        if (choose == '1') {//1 เปลี่ยนค่า
            this.name = getN();
            this.shape = getS();
            this.feature = getF();
            this.model = model;

        } else if (choose == '0') {//0 ไม่เปลี่ยนค่า
            this.name = name;
            this.shape = shape;
            this.feature = feature;
            this.model = model;
        }
        switch (model) {
            case "B":
                efficacy = 60;
                break;
            case "C":
                efficacy = 70;
                break;
            case "D":
                efficacy = 80;
                break;
            case "E":
                efficacy = 90;
                break;
            default:
                return efficacy;
        }
        return efficacy;
    }
}
