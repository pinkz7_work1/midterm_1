/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.midterm01;

/**
 *
 * @author ripgg
 */
public class TestGlasses {
    public static void main(String[] args) {
        Glasses glasses1 = new Glasses("A01E","Round","Bluelightfilter","E");
        System.out.println("Name:"+glasses1.name+" Shape: "+glasses1.shape+" Feature:"+glasses1.feature+" Model: "+glasses1.model+" Efficacy: "+glasses1.efficacy);
        glasses1.setG("A01E","Round","Bluelightfilter","D",'1');
        System.out.println("Name:"+glasses1.name+" Shape: "+glasses1.shape+" Feature:"+glasses1.feature+" Model: "+glasses1.model+" Efficacy: "+glasses1.efficacy);
        glasses1.setG("A02R","Rectangle","Sunglasses","B",'0');
        System.out.println("Name:"+glasses1.name+" Shape: "+glasses1.shape+" Feature:"+glasses1.feature+" Model: "+glasses1.model+" Efficacy: "+glasses1.efficacy);
        System.out.println();
        Glasses glasses2 = new Glasses("C04A","Square","Blulightfilter","C");
        System.out.println("Name:"+glasses2.name+" Shape: "+glasses2.shape+" Feature:"+glasses2.feature+" Model: "+glasses2.model+" Efficacy: "+glasses2.efficacy);
        glasses2.setG("A02C","Sport","Sunglasses","D",'0');
        System.out.println("Name:"+glasses2.name+" Shape: "+glasses2.shape+" Feature:"+glasses2.feature+" Model: "+glasses2.model+" Efficacy: "+glasses2.efficacy);
        System.out.println();
        Glasses glasses3 = new Glasses("F11J","Pilot","Sunglasses","A");
        System.out.println("Name:"+glasses3.name+" Shape: "+glasses3.shape+" Feature:"+glasses3.feature+" Model: "+glasses3.model+" Efficacy: "+glasses3.efficacy);
        glasses3.setG("Q17G","Round","Sunglasses","B",'0');
        System.out.println("Name:"+glasses3.name+" Shape: "+glasses3.shape+" Feature:"+glasses3.feature+" Model: "+glasses3.model+" Efficacy: "+glasses3.efficacy);
        System.out.println();
    }
}
